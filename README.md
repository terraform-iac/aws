# Terraform GitLab CI/CD Pipeline

This repository contains the GitLab CI/CD pipeline configuration for managing your Terraform infrastructure using GitLab CI/CD. The pipeline includes stages for validation, planning, applying, and destroying your Terraform resources.

## Prerequisites

- You need a GitLab project where you can configure this CI/CD pipeline.
- Ensure you have the necessary AWS credentials (AWS access key and secret access key) set as environment variables.
- The Terraform files are organized in the directory specified by the `TF_ROOT` variable.

## Pipeline Configuration

The pipeline is defined in the `.gitlab-ci.yml` file. It's designed to execute Terraform commands for managing infrastructure.

### Variables

The following variables are used in the pipeline configuration:

- `TF_IN_AUTOMATION`: Set to `"true"` to indicate Terraform is running in an automated environment.
- `GITLAB_TF_ADDRESS`: The GitLab Terraform state backend address.
- `STATE`: The name of the Terraform state file.
- `TF_ROOT`: The directory containing your Terraform configuration.

### Stages

The pipeline is divided into the following stages:

- **validate**: Validates the syntax and configuration of your Terraform files.
- **plan**: Generates a Terraform plan and stores it in the `planfile` artifact.
- **apply**: Manually applies the generated Terraform plan to create or update resources.
- **destroy**: Manually destroys the Terraform-managed resources.

## How to Use

1. Make sure your Terraform configuration is organized within the `TF_ROOT` directory.
2. Configure your AWS credentials as environment variables within your GitLab CI/CD settings.
3. Update the `GITLAB_TF_ADDRESS` variable with the appropriate GitLab Terraform state backend address.
4. Push your changes to the GitLab repository.

The pipeline will automatically execute when you push changes to your repository. You can also manually trigger the `apply` and `destroy` stages.

## Important Notes

- Ensure your AWS credentials are kept secure and not exposed in the repository.
- Review the Terraform state backend configuration to ensure secure state management.
- The `apply` and `destroy` stages are set to manual execution to prevent accidental changes. You can manually trigger these stages from the GitLab CI/CD pipeline dashboard.

