module "aws_ec2" {
  source          = "../modules/ec2-instance/"
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = var.create_key ? module.aws_ssh_key.get_key_name : "myEC2Key"
  security_groups = module.aws_security_group.get_security_group_name
  tags            = var.tags
  user_data       = file("${path.module}/jenkins.sh")
}

module "aws_security_group" {
  source = "../modules/security-group"
  name   = "${var.name}-${module.random_number.random_values}"
}

module "aws_ssh_key" {
  count    = local.run_script_enabled ? 1 : 0
  source   = "../modules/ssh-key"
  key_name = var.key_name
}

module "random_number" {
  source = "../modules/random"
}

resource "null_resource" "run_script" {
  count = local.run_script_enabled ? 1 : 0
  provisioner "local-exec" {
    command = <<-EOT
        echo '${module.aws_ssh_key[0].private_key}' > ./${module.random_number.random_values}.pem
        echo '${module.aws_ssh_key[0].public_key}' > ./${module.random_number.random_values}.pub
        chmod 400 ./${module.random_number.random_values}.pem
    EOT
  }
}

