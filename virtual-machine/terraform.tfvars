tags          = "EC2"
ami           = "ami-0729e439b6769d6ab"
instance_type = "t2.medium"
key_name      = "terraform-key"
name          = "terraform-security-group"
region        = "us-east-1"
create_key    = false