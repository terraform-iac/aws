## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.20.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_null"></a> [null](#provider\_null) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_aws_ec2"></a> [aws\_ec2](#module\_aws\_ec2) | ../modules/ec2-instance/ | n/a |
| <a name="module_aws_security_group"></a> [aws\_security\_group](#module\_aws\_security\_group) | ../modules/security-group | n/a |
| <a name="module_aws_ssh_key"></a> [aws\_ssh\_key](#module\_aws\_ssh\_key) | ../modules/ssh-key | n/a |
| <a name="module_random_number"></a> [random\_number](#module\_random\_number) | ../modules/random | n/a |

## Resources

| Name | Type |
|------|------|
| [null_resource.run_script](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami"></a> [ami](#input\_ami) | n/a | `string` | n/a | yes |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | n/a | `string` | n/a | yes |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | n/a | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
