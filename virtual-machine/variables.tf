variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "name" {
  type = string
}

variable "key_name" {
  type = string
}

variable "tags" {
  type = string
}

variable "region" {
  type = string
}

variable "create_key" {
  type = bool
}