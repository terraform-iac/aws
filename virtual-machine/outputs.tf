output "get_public_key" {
  value     = var.create_key ? module.aws_ssh_key.public_key : null
  sensitive = true
}

output "get_private_key" {
  value     = var.create_key ? module.aws_ssh_key.private_key : null
  sensitive = true
}


output "ssh_to_vm" {
  value = var.create_key ? "ssh -i './${module.random_number.random_values}.pem' ubuntu@${module.aws_ec2.get_dynamic_ip}" : null
}

