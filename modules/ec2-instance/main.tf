resource "aws_instance" "instance" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name
  tags = {
    "Name" = var.tags
  }
  # vpc_security_group_ids = var.vpc_security_group_ids
  security_groups = var.security_groups
  user_data = var.user_data
}
