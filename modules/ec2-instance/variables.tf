variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "tags" {
  type = string
}

variable "security_groups" {
  type    = list(string)
  default = [""]
}

variable "user_data" {
  type = string
}