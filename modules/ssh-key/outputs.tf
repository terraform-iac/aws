output "get_key_name" {
  value = aws_key_pair.key_pair.key_name
}

output "private_key" {
  value = tls_private_key.private_key.private_key_pem
}

output "public_key" {
  value = tls_private_key.private_key.public_key_pem
}

